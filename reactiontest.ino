/*
reaction_tester_2.ino

For (arduino <--> lcd) pin configuration check, changes to pins 2 and 3 to free interrupts
https://www.arduino.cc/en/Tutorial/HelloWorld

  LCD   -->   Arduino Uno
  RS    -->   12
  Enable  -->   11
  D4    -->   5
  D5    -->   4
  D6    -->   9
  D7    -->   6
  R/W   -->   GND
  VSS   -->   GND
  VCC   -->   5V
  pin15(A)-->   220ohm resistor --> 5V
  pin16(K)-->   GND
  Between +5V and GND
    10k pot, wiper to LCD V0
    
  10k pot --> lcd V0 (between +5V and GND) for lcd contrast
  lcd pins 15, 16 and 220ohm resistor between +5V and GND to power backlight


TO DO:

Scrollable screen for WIN state to display both players and high score
ifdef Serial debug

PROBLEMS: 
(in WIN state, first shown screen is random, not the one showing reaction time)

*/

#include "LiquidCrystal.h"
#include "states.h"
// Pins for buttons, onboard LED and LCD pins,
// pins 2 and 3 on Uno are interruptable
#define BT1 2
# define BT2 3
# define LED 13
# define RS 12
# define ENABLE 11
# define D4 5
# define D5 4
# define D6 9
# define D7 6
# define PIEZO 10

LiquidCrystal lcd(RS, ENABLE, D4, D5, D6, D7);
// States explained in doc "SulautetutHT1Doc.txt"

game_state state = START;
int counter;
// global variables changed by interrupts require volatile
volatile int bt1;
volatile int bt2;
int rndTime;
unsigned long strtTime;
unsigned long endTime;
int bt1time; // init with impossible number to chec if already pressed
int bt2time;
int winTime;
int botreact;
int highscore;
String winner;
boolean safe;
boolean buttonsafety;
boolean mode;
unsigned long currentMillis;


void initState() {
    /* Clear lcd screen on first loop of a state and set cursor to 0,0 on every loop*/
    if (counter == 0) {
        lcd.clear();
        counter = 1;
        if (state == REACT) {
            strtTime = millis();
            endTime = strtTime + rndTime;
            //Serial.print("initstate, endtime maaritetty: ");
            //Serial.println(endTime);
            //Serial.print("strtTime: ");
            //Serial.println(strtTime);
            //Serial.print("rndTime: ");
            //Serial.println(rndTime);
            botreact = endTime + random(200, 800);
        }
        if (state == WIN) {

            attachInterrupt(digitalPinToInterrupt(BT1), WinBT1, RISING);
            attachInterrupt(digitalPinToInterrupt(BT2), WinBT2, RISING);
            //Serial.println("Setting win safe ON");
            safe = true;
        }
        // debounce button on state change
        buttonsafety = true;
    }
    lcd.setCursor(0, 0);

}

void stateEnd(game_state s) {
    /* Must be used before transitioning to another state*/
    counter = 0;
    state = s;
}

void bt1press() {
    if (safe) {
        if (millis() - strtTime > 1000) {
            bt1time = millis() - endTime;
        }
    }
}

void bt2press() {
    if (safe) {
        if (millis() - strtTime > 1000) {
            bt2time = millis() - endTime;
        }
    }
}

void WinBT1() {
    if (safe) {
        
        stateEnd(READY);
        safe = false;
        //Serial.println("Setting win safe OFF");
        detachInterrupt(digitalPinToInterrupt(BT1));
        detachInterrupt(digitalPinToInterrupt(BT2));
    }
}

void WinBT2() {
    if (safe) {
        detachInterrupt(digitalPinToInterrupt(BT1));
        stateEnd(START);
        safe = false;
        //Serial.println("Setting win safe OFF");
        detachInterrupt(digitalPinToInterrupt(BT2));
    }
}

void setup() {
    // Init lcd and show welcome screen for 500ms
    lcd.begin(16, 2);
    lcd.print("Reaction time");
    lcd.setCursor(0, 1);
    lcd.print("  tester v0.1");
    pinMode(LED, OUTPUT);
    pinMode(BT1, INPUT);
    pinMode(BT2, INPUT);
    delay(1000);

    // Init global variables
    counter = 0;
    safe = false;
    state = START;
    bt1 = LOW;
    bt2 = LOW;
    mode = false;
    rndTime = 5000;
    strtTime = 0;
    winner = "";
    bt1time = 50000;
    bt2time = 50000;
    winTime = 0;
    Serial.begin(9600);
    highscore = 10000;
}

void loop() {

    switch (state) {

        // Start game and ask player number
    case START:
        initState();
        if (buttonsafety) {
            delay(500); //So depressed button doesn't trigger moving on
            buttonsafety = false;
        }
        lcd.print("BT1=Multiplayer");
        lcd.setCursor(0, 1);
        lcd.print("BT2=Singleplayer");
        if (digitalRead(BT1)) {
            mode = false;
            stateEnd(READY);
        } else if (digitalRead(BT2)) {
            mode = true;
            stateEnd(READY);
        }
        //Serial.println("@casestart");  

        break;

        // Start react time and go to REACT on any btn press
    case READY:
        initState();

        bt1time = 50000;
        bt2time = 50000;
        lcd.print("     Ready?");
        lcd.setCursor(0, 1);
        lcd.print("Press any button");
        if (buttonsafety) {
            delay(500); //So depressed button doesn't trigger moving on
            buttonsafety = false;
            //Serial.println("@bs while");
        }

        if (digitalRead(BT1) || digitalRead(BT2)) { ///Sense button press once button has been up once
            rndTime = random(2000, 8000);
            stateEnd(REACT);
            //delay(400); 
            attachInterrupt(digitalPinToInterrupt(BT1), bt1press, RISING);
            if (mode == false) {
                attachInterrupt(digitalPinToInterrupt(BT2), bt2press, RISING);
            }
            //Serial.println("Setting react safe ON");
            safe = true;
            strtTime = millis();
        }

        break;

        // Save times when interrupts trigger while showing the "HIT" screen and playing sound
    case REACT:
        initState();

        // Since millis() disables interrupts while it gets time might be questionable structure
        // for measuring reaction time
        while (millis() < endTime) {
            lcd.setCursor(0, 0);
            lcd.print("      hold");
        }
        while (millis() >= endTime) {

            lcd.setCursor(0, 0);
            lcd.print("      HIT!");
            if (bt1time != 50000 || bt2time != 50000) {
                break;
            }
            if (millis() >= botreact && mode == true) {
                bt2press();
            }
        }

        detachInterrupt(digitalPinToInterrupt(BT1));
        if (mode == false) {
            detachInterrupt(digitalPinToInterrupt(BT2));
        }
        //Serial.println("Setting react safe OFF");
        safe = false;
        stateEnd(WIN);
        break;
        // On BT1 press go again, on BT2 press choose players again
        // Show reaction time and additional info, options to go to START or READY
    case WIN:
        if (buttonsafety) {
            delay(500); //So depressed button doesn't trigger moving on
            buttonsafety = false;
        }
        initState();
        currentMillis = millis();
        if (currentMillis > 6000) {
            currentMillis = currentMillis % 6000;
        }
        if (currentMillis >= 2000 && currentMillis < 4000) {
            if (bt1time < bt2time && bt1time > 0) {
                if (mode == true) {
                    winner = "You win!  ";
                } else {
                    winner = " Player 1 wins!";
                }
                winTime = bt1time;
            } else if (bt2time < bt1time && bt2time > 0) {
                if (mode == true) {
                    winner = " Computer wins!";
                } else {
                    winner = " Player 2 wins!";
                }
                winTime = bt2time;
            } else if (bt2time == bt1time && bt1time > 0) {
                winner = "      Draw";
                winTime = bt1time;
            } else {
                winner = "other win msg";
            }
            if (winTime < highscore) {
                highscore = winTime;
            }
            lcd.print(winner);
            lcd.setCursor(0, 1);
            if (winTime == 50000) {
                lcd.print("Invalid");
            } else {
                lcd.print(winTime);
            }
            lcd.print(" ms           ");
            //Serial.println("@bs while");
        } else if (currentMillis < 2000) {
            lcd.print("BT1=Again      ");
            lcd.setCursor(0, 1);
            lcd.print("BT2=Main menu");
        } else {
            lcd.print("Highscore:       ");
            lcd.setCursor(0, 1);
            lcd.print(highscore);
            lcd.print(" ms           ");
            //Serial.print("End of WIN loop");
            //Serial.println(safe);
            break;
            default:
            break;
        }
    }
}
