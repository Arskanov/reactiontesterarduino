/*
Workaround to be able to use game_state as function parameter and returntype
*/

enum game_state{
	START,
	READY,
	REACT,
	WIN
};